SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE TABLE `basedoc` (
  `doc_id` int(11) NOT NULL auto_increment,
  `doc_titre` varchar(100) default NULL,
  `doc_date_creat` datetime default NULL,
  `doc_date_modif` datetime default NULL,
  `doc_texte` text,
  `user_id` int(11) default NULL,
  PRIMARY KEY  (`doc_id`),
  KEY `FK_basedoc_user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `college` (
  `clg_uai` varchar(10) NOT NULL,
  `clg_nom` varchar(255) DEFAULT NULL,
  `clg_ati` varchar(255) DEFAULT NULL,
  `clg_ati_mail` varchar(255) DEFAULT NULL,
  `clg_adresse` varchar(255) DEFAULT NULL,
  `clg_cp` varchar(20) DEFAULT NULL,
  `clg_ville` varchar(255) DEFAULT NULL,
  `clg_tel` varchar(20) DEFAULT NULL,
  `clg_fax` varchar(20) DEFAULT NULL,
  `clg_site_web` varchar(255) DEFAULT NULL,
  `clg_site_grr` varchar(255) DEFAULT NULL,
  `clg_nbSalles` int(11) NOT NULL,
  `clg_nbEleves` int(11) NOT NULL,
  `clg_appAbsence` varchar(255) NOT NULL,
  `clg_appNote` varchar(255) NOT NULL,
  `clg_appCahiertxt` varchar(255) NOT NULL,
  `clg_appCdi` varchar(255) NOT NULL,
  PRIMARY KEY (`clg_uai`),
  UNIQUE KEY `clg_uai` (`clg_uai`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `demandes` (
  `dem_id` int(11) NOT NULL auto_increment,
  `dem_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `dem_text` text,
  `dem_etat` varchar(100) NOT NULL,
  `dem_type` varchar(30) NOT NULL,
  `user_demandeur_id` int(11) default NULL,
  `user_intervenant_id` int(11) default NULL,
  `mat_id` int(11) default NULL,
  `salle_id` int(11) default NULL,
  PRIMARY KEY  (`dem_id`),
  KEY `FK_demandes_user_demandeur_id` (`user_demandeur_id`),
  KEY `FK_demandes_mat_id` (`mat_id`),
  KEY `FK_demandes_salle_id` (`salle_id`),
  KEY `FK_demandes_user_intervenant_id` (`user_intervenant_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE `demandes_textes` (
  `txt_id` int(11) NOT NULL auto_increment,
  `txt_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `txt_etat` varchar(30) NOT NULL,
  `txt_texte` text NOT NULL,
  `dem_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY  (`txt_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE `etats` (
  `etat` varchar(255) NOT NULL,
  PRIMARY KEY  (`etat`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `interventions` (
  `interv_id` int(11) NOT NULL auto_increment,
  `interv_date` timestamp NULL default CURRENT_TIMESTAMP,
  `interv_cloture` timestamp NULL default NULL,
  `interv_text` text,
  `dem_id` int(11) default NULL,
  `salle_id` int(11) default NULL,
  `mat_id` int(11) default NULL,
  `user_id` int(11) default NULL,
  PRIMARY KEY  (`interv_id`),
  KEY `FK_interventions_dem_id` (`dem_id`),
  KEY `FK_interventions_salle_id` (`salle_id`),
  KEY `FK_interventions_mat_id` (`mat_id`),
  KEY `FK_interventions_user_id` (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE `logs` (
  `log_id` int(11) NOT NULL auto_increment,
  `log_type` varchar(30) NOT NULL,
  `log_date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `log_texte` text NOT NULL,
  PRIMARY KEY  (`log_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE `marques` (
  `marque_id` int(11) NOT NULL auto_increment,
  `marque_marque` varchar(255) NOT NULL,
  `marque_model` varchar(255) default NULL,
  `marque_type` varchar(255) default NULL,
  `marque_stype` varchar(255) NOT NULL,
  `marque_suppr` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`marque_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE `materiels` (
  `mat_id` int(11) NOT NULL auto_increment,
  `mat_nom` varchar(255) default NULL,
  `mat_dsit` varchar(100) default NULL,
  `mat_serial` varchar(100) default NULL,
  `mat_mac` varchar(17) default NULL,
  `mat_etat` varchar(100) default 'Fonctionnel',
  `mat_origine` varchar(7) NOT NULL,
  `salle_id` int(11) NOT NULL default '1',
  `user_id` int(11) default '1',
  `marque_id` int(11) default NULL,
  `mat_suppr` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`mat_id`),
  UNIQUE KEY `mat_serial` (`mat_serial`),
  KEY `FK_materiels_salle_id` (`salle_id`),
  KEY `FK_materiels_user_id` (`user_id`),
  KEY `FK_materiels_marque_id` (`marque_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE `origines` (
  `origine` varchar(20) NOT NULL default 'INCONNUE',
  PRIMARY KEY  (`origine`),
  UNIQUE KEY `origine` (`origine`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `salles` (
  `salle_id` int(11) NOT NULL auto_increment,
  `salle_nom` varchar(80) default NULL,
  `salle_vlan` varchar(30) default NULL,
  `salle_etage` varchar(30) default NULL,
  `salle_batiment` varchar(30) default NULL,
  `clg_uai` varchar(10) default NULL,
  `salle_suppr` tinyint(1) NOT NULL default '0',
  `est_modifiable` tinyint(1) NOT NULL default '1',
  PRIMARY KEY  (`salle_id`),
  UNIQUE KEY `salle_nom` (`salle_nom`),
  KEY `FK_salles_clg_uai` (`clg_uai`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL auto_increment,
  `user_nom` varchar(255) default NULL,
  `user_logon` varchar(20) NOT NULL,
  `user_password` varchar(15) default NULL,
  `grade_id` int(11) default '3',
  `user_skin` varchar(150) NOT NULL default 'cg13',
  `user_accueil` varchar(255) NOT NULL default 'modules/stats/csschart.php',
  `user_mail` varchar(100) NOT NULL,
  `user_mailing` tinyint(1) NOT NULL default '0',
  `user_suppr` tinyint(1) NOT NULL,
  `est_modifiable` tinyint(1) NOT NULL default '1',
  PRIMARY KEY  (`user_id`),
  UNIQUE KEY `user_logon` (`user_logon`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;


CREATE TABLE `correspondances` (
  `corr_id` int(11) NOT NULL auto_increment,
  `corr_marque_ocs` varchar(255) NOT NULL,
  `corr_type` varchar(255) NOT NULL,
  `corr_stype` varchar(255) NOT NULL,
  `corr_marque` varchar(255) NOT NULL,
  `corr_modele` varchar(255) NOT NULL,
  PRIMARY KEY  (`corr_id`),
  UNIQUE KEY `corr_marque_ocs` (`corr_marque_ocs`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE `grades` (
  `grade_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `grade_nom` varchar(255) NOT NULL,
  `grade_menu` text NOT NULL,
  `grade_menu_portail` text NOT NULL,
  `est_modifiable` tinyint(1) NOT NULL default '1',
  PRIMARY KEY (`grade_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE `menu_portail` (
  `mp_id` int(11) NOT NULL AUTO_INCREMENT,
  `mp_nom` varchar(255) NOT NULL,
  `mp_url` varchar(255) NOT NULL,
  `mp_icone` varchar(255) NOT NULL,
  `est_modifiable` tinyint(1) NOT NULL default '1',
  PRIMARY KEY (`mp_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

CREATE TABLE IF NOT EXISTS `dossiers` (
  `dossier_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dossier_type` varchar(255) NOT NULL,
  `dossier_mat` text NOT NULL,
  `dossier_mailing` tinyint(1) NOT NULL default '1',
  PRIMARY KEY (`dossier_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `dossiers_textes` (
  `txt_id` int(11) NOT NULL AUTO_INCREMENT,
  `dossier_id` int(11) NOT NULL,
  `txt_user` int(11) NOT NULL,
  `txt_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `txt_texte` text NOT NULL,
  `txt_etat` varchar(255) NOT NULL,
  PRIMARY KEY (`txt_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `droits` (
  `droit_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `droit_index` varchar(5) NOT NULL,
  `droit_titre` varchar(255) NOT NULL,
  `droit_page` varchar(255) NOT NULL,
  `droit_etendue` tinyint(1) NOT NULL DEFAULT '1',
  `droit_description` varchar(255) NOT NULL,
  PRIMARY KEY (`droit_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `fichiers` (
  `fichier_id` int(11) NOT NULL AUTO_INCREMENT,
  `fichier_chemin` varchar(255) NOT NULL,
  `fichier_description` text NOT NULL,
  `fichier_droits` varchar(2) NOT NULL DEFAULT '00',
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`fichier_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `dossiers_types` (
  `type` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
