#!/bin/bash




read -p "You gonna STOP and REMOVE ALL containers  (y/n)? " answer
case ${answer:0:1} in
    y|Y )

        # stop all containers
        echo "Stoping all containers..."
        sudo docker stop $(sudo docker ps -a -q)

        # remove all containers
        echo "Removing all containers..."
        sudo docker rm $(sudo docker ps -a -q)




        read -p "Remove <NONE> images (y/n)? " answer
        case ${answer:0:1} in
            y|Y )
                # remove dangling images
                echo "Removing <NONE> images..."
                sudo docker rmi -f $(sudo docker images -a | grep "<none>" | awk "{print \$3}")
            ;;
        esac


        read -p "Remove DANGLING images (y/n)? " answer
        case ${answer:0:1} in
            y|Y )
                # remove dangling images
                echo "Removing DANGLING images..."
                sudo docker rmi -f $(sudo docker images -f "dangling=true" -q)
            ;;
        esac



        read -p "Remove ALL other images (y/n)? " answer
        case ${answer:0:1} in
            y|Y )
                # remove all images
                echo "Removing ALL images..."
                sudo docker rmi $(sudo docker images -a | awk "{print \$3}")
            ;;
        esac

    ;;
    * )
        read -p "Bye ! [ENTER]"
    ;;
esac
