#!/bin/bash

###########################################################
# MAKE A .env FILE with Database name, username, password #
###########################################################


# remove the file
rm -f .env
# make a generic one
echo -e "DB_USERNAME=username\nDB_PASSWORD=password\nDB_DATABASE=gespac" > .env


# change a config field
function set_config(){
    sudo sed -i "s/^\($1\s*=\s*\).*\$/\1$2/" .env
}

# ask for data
read -p "Database NAME (default:gespac) [ENTER]:" DB_DATABASE
read -p "Database USERNAME (default:root) [ENTER]:" DB_USERNAME
read -p "Database PASSWORD [ENTER]:" DB_PASSWORD

while [ -z $DB_PASSWORD ]; do
    echo "!! Password cannot be empty !!"
    read -p "Database PASSWORD [ENTER]:" DB_PASSWORD
done

# default values
DB_DATABASE=${DB_DATABASE:-gespac}
DB_USERNAME=${DB_USERNAME:-root}

echo

# Check if correct
echo "Please check if all fields are correct :"
echo "DB_DATABASE : " $DB_DATABASE
echo "DB_USERNAME : " $DB_USERNAME
echo "DB_PASSWORD : " $DB_PASSWORD

echo


read -p "Save configuration file (y/n)? " answer
case ${answer:0:1} in
    y|Y )
        set_config "DB_DATABASE" $DB_DATABASE
        set_config "DB_USERNAME" $DB_USERNAME
        set_config "DB_PASSWORD" $DB_PASSWORD
        echo "Done !"
    ;;
    * )
        read -p "See ya ! [ENTER]"; exit
    ;;
esac


echo


read -p "Run Gespac image (y/n)? " answer
case ${answer:0:1} in
    y|Y )
        sudo docker-compose up --build --force-recreate -d
        echo "Done !"
        echo
        echo "adminer url : http://localhost:8080"
        echo "gespac url : http://localhost:81/gespac"

        read -p "Press any key to continue..."
    ;;
    * )
        read -p "See ya ! [ENTER]"
    ;;
esac
