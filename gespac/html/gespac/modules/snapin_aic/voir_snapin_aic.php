<?PHP session_start(); ?>

<!--
	Visualisation des PC à migrer dans FOG
	On sélectionne les PC dans la liste et on met
	à jour dans le post les noms des machines dans FOG.

-->


<script>
	// Fonction de validation de la suppression d'une marque
	function validation_suppr(id, nom) {

		var valida = confirm('Voulez-vous vraiment supprimer le snapin ' + nom + ' ?');
		
		// si la réponse est TRUE ==> on lance la page post_marques.php
		if (valida) {
			$('#targetback').show(); $('#target').show();
			$('#target').load("modules/snapin_aic/post_snapin_aic.php?action=suppr&id=" + id);
			window.setTimeout("document.location.href='index.php?page=aic'", 2500);		
		}
	}
</script>


<?PHP
	
	// @@Gestion des droits sur la page
	$pageid = "07-13";	
	if ( !preg_match ("#L-$pageid#", $_SESSION['droits']) && $_SESSION['grade'] != 'root' ) exit("<div style='font-size:40px;margin:60px;'>Vous n'avez pas les droits d'accès à cette page !</div>");	// Droit d'accès en lecture à la page
	$E_chk = ($_SESSION['grade'] == 'root') ? true : preg_match ("#E-$pageid#", $_SESSION['droits']);	// Droit d'accès en écriture à la page

?>


<div class="entetes" id="entete-aic">	

	<span class="entetes-titre">SNAPINS FOG pour AIC <i class="icon entetes-icon ion-help-circled help-button"></i></span>
	<div class="helpbox">Afin d'éviter de créer un fichier AIC par OU de l'AD, on peut déployer le même fichier AIC.EXE avec des paramètres.<br>Cette page permet de créer dans fog un snapin paramétré pour intégrer les machines au domaine.</div>

	<span class="entetes-options">
		
		<span class="option">
			<!-- 	bouton pour le filtrage du tableau	-->
			<form id="filterform">
				<input placeholder=" filtrer" name="filt" id="filt" onKeyPress="return disableEnterKey(event)" onkeyup="filter(this.value, 'association_uo');" type="text"> 
				<span id="nb_filtre" title="nombre de lignes filtrées"></span>
			</form>
		</span>
	</span>

</div>

<div class="spacer"></div>

<?PHP

	echo "<center>";
	
	if ($E_chk) {
		
		if (file_exists("/opt/fog/snapins/aic.exe")) {
			echo "<a href='index.php?page=aicform'>Gérer les snapins AIC</a>";
		} else {
			echo "Le fichier <b>aic.exe</b> (attention à la casse) n'existe pas. Merci de créer un snapin avec ce fichier dans FOG.<br>";
			echo "<a href='index.php?page=aicform'>Je posterai mon snapin aic.exe plus tard.</a><br>";
		}
		
	} else {
		echo "Vous n'avez pas les droits suffisants.";
	}
	
	echo "<br><br></center>";
	
	
	// cnx à fog
	$con_fog = new Sql($host, $user, $pass, $fog);
	
	// rq pour la liste des PC
	$liste_snapins_fog = $con_fog->QueryAll ("SELECT sID, sName, sDesc, sFilePath, sArgs FROM snapins WHERE sFilePath='/opt/fog/snapins/aic.exe';");
	
			
	/*************************************
	*
	*		LISTE DE SELECTION
	*
	**************************************/

	echo "<table id='association_uo' class='hover bigtable'>";

	echo "
		<th>Snapin</th>
		<th>Arguments</th>
		<th>&nbsp;</th>
	";

	foreach ($liste_snapins_fog as $record) {
		
		$sID		= $record['sID'];
		$sName		= utf8_encode($record['sName']);
		$sDesc 		= utf8_encode($record['sDesc']);
		$sFilePath 	= $record['sFilePath'];
		$sArgs		= $record['sArgs'];
		

		echo "<tr id=tr_id$hostid >";
			
			echo "<td>$sName</td>
			<td>$sArgs</td>
			<td> <a href='#' onclick=\"javascript:validation_suppr($sID, '$sName');\"><i class='icon table-icon ion-close-round'></i></a> </td>
		</tr>";
		
	}
	
	echo "</table>";	

?>
