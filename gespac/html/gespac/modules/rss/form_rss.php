<?PHP

	$action = $_GET['action'];

	if ( $action == 'ajout' ) {

?>	
	
		<form action="modules/rss/post_rss.php?action=add" method="post" name="post_add_flux_rss" id="formulaire">
		
			<center>
			<table class="formtable">
			
				<tr>
					<TD>Nom du flux *</TD>
					<TD><input type="text" name="nom" id="nom" class="valid nonvide"></TD>
				</tr>
				
				<tr>
					<TD>URL du flux *</TD>
					<TD><input type="text" name="url" id="url" class="valid nonvide url"></TD>
				</tr>
				
			</table>

			<br>
			<input type=submit value='Ajouter le flux' id="post_form">

			</center>

		</FORM>
	
	
	
<?PHP	
	}
?>


	<script type="text/javascript" src="../../js/main.js"></script>

	<script>
		// Donne le focus au premier champ du formulaire
		$('#nom').focus();


		$(function() {	
					
			// **************************************************************** POST AJAX FORMULAIRES
			$("#post_form").click(function(event) {

				/* stop form from submitting normally */
				event.preventDefault(); 
				
				if ( validForm() == true) {
				
					// Permet d'avoir les données à envoyer
					var dataString = $("#formulaire").serialize();
					
					// action du formulaire
					var url = $("#formulaire").attr( 'action' );
					
					var request = $.ajax({
						type: "POST",
						url: url,
						data: dataString,
						dataType: "html"
					 });
					 
					 request.done(function(msg) {
						$('#dialog').dialog('close');
						$('#targetback').show(); $('#target').show();
						$('#target').html(msg);
						window.setTimeout("document.location.href='index.php?page=rss'", 2500);
					 });
				}			 
			});	
		});
		
	</script>
