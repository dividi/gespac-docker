<?PHP
	session_start();

	// @@Gestion des droits sur la page
	$pageid = "08-02";	
	if ( !preg_match ("#L-$pageid#", $_SESSION['droits']) && $_SESSION['grade'] != 'root' ) exit("<div style='font-size:40px;margin:60px;'>Vous n'avez pas les droits d'accès à cette page !</div>");	// Droit d'accès en lecture à la page
	$E_chk = ($_SESSION['grade'] == 'root') ? true : preg_match ("#E-$pageid#", $_SESSION['droits']);	// Droit d'accès en écriture à la page
?>


<div class="entetes" id="entete-statparc">	
	<span class="entetes-titre">FLUX RSS <i class="icon entetes-icon ion-help-circled help-button"></i></span>
	<div class="helpbox">Cette page permet de s'abonner et de lire des flux RSS/ATOM.</div>

	<span class="entetes-options">
		<?PHP
		
		if ( $handle = @fopen("dump/flux.txt", "r") ) {
			
			$row = 0;
			$current_flux = $_GET['flux'];
			
			echo "<span class=option><select id='select_flux' onchange=\"document.location.href='index.php?page=rss&flux=' + $(this).val();  \">";
			
				while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
					
					$line[$row][0] = $data[0];	
					$line[$row][1] = $data[1];		
					
					$selected = $row==$current_flux ? " selected" : "" ;
										
					echo "<option value=$row $selected>" . $line[$row][0] . "</option>";

					$row++;
				}
			
			echo "</select></span>";
		
			fclose ($handle);
		}
				
		if ( $E_chk ) {
			// si le fichier flux n'existe pas, on ne permet pas la suppression (et la suppression de quoi d'abord ?)
			if ( $row > 0 )	echo "<span class='option'><a href='#' onclick='supprimer_flux( $(\"#select_flux\").val() );' title='Supprimer le flux'><i class='icon entetes-icon ion-minus-circled'></i></a></span>";
			
			echo "<span class='option'><a href='modules/rss/form_rss.php?action=ajout' class='editbox' title='Ajouter un flux'><i class='icon entetes-icon ion-plus-circled'></i></a></span>";
		}
		
		?>

	</span>

</div>

<div class="spacer"></div>


<?PHP	
	// On charge la page des flux
	include ('rss_flux.php');
?>	

<script type="text/javascript"> 

	// Permet de supprimer un flux
	function supprimer_flux (ligne) {
		
		var valida = confirm('Voulez-vous vraiment supprimer ce flux ?');
		
		if ( valida ) {
			$('#targetback').show(); $('#target').show();
			$('#target').load("modules/rss/post_rss.php?action=suppr&id=" + ligne);
			window.setTimeout("document.location.href='index.php?page=rss'", 2500);		
		}
	};
	
</script>
