<?PHP

	// lib
	require_once ('../../fonctions.php');
	include_once ('../../config/databases.php');
	include_once ('../../../class/Log.class.php');
	include_once ('../../../class/Sql.class.php');

	$export_champ 	= $_POST ['export_champ'];
	$maj_desc 		= $_POST ['maj_desc'];
	$lot 			= $_POST ['id_a_poster'];
	$lot_array 		= explode(";", $lot);


	// cnx à la db gespac
	$con_gespac = new Sql($host, $user, $pass, $gespac);
	
	// Log des requêtes SQL
	$log = new Log ("../../dump/log_sql.sql");
	
	$liste = "";

	// On constitue la requête
	foreach ($lot_array as $machine) {
		if ( $machine <> "" ) $liste .= " OR mat_id=$machine";
	}
	
	// rq pour la liste des serial + inventaire
	$pc_gespac = $con_gespac->QueryAll ("SELECT mat_serial, mat_dsit, mat_nom FROM materiels WHERE mat_id='' $liste");
	
	
	// cnx à la db fog
	$con_fog = new Sql($host, $user, $pass, $fog);	
	
	foreach ($pc_gespac as $pc) {
			
		$gespac_serial = $pc['mat_serial'];
		$gespac_dsit = $pc['mat_dsit'];
		$gespac_nom = $pc['mat_nom'];
		
		$field = $export_champ == "nom" ? $gespac_nom : $gespac_dsit;
				
		// On récupère les hostIDs grace au serial
		$hostIDs = $con_fog->QueryAll ("SELECT iHostID FROM inventory WHERE iSysserial='$gespac_serial'");
		
		foreach ( $hostIDs as $hostID ) {
		
			$id = $hostID["iHostID"];	

			if ($field <> "") {	// Le cas où le tag ou le nom est nul (bon le nom c'est pas possible, mais bon)
				if ( $maj_desc ) {
					$sql = "UPDATE hosts SET hostName = '$field', hostDesc = '$gespac_nom' WHERE hostID=$id";
					$con_fog->Execute($sql);
					$log->Insert($sql);
				}
				else {
					$sql = "UPDATE hosts SET hostName = '$field' WHERE hostID=$id";
					$con_fog->Execute($sql);
					$log->Insert($sql);
				}
			}
		}

	}
	
	echo "Migration des noms des machines dans FOG effectuée !";
	

?>
