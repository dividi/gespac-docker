<?PHP 
	session_start();

/*
	menu en haut à gauche
	TODO: faire disparaitre les menus si l'utilisateur n'a pas les droits.
*/

?>

<div id="menu-global">

	<div class="menu-block" id="menu-accueil">
		<div class="menu-titre" id="entete-menu-accueil"><i class="icon menu-icon ion-ios7-home"></i>ACCUEIL</div>
		<div class="menu-items">
			<a id="01-01" href="index.php?page=accueil"><div class="menu-item" id='accueil'>Retour à l'accueil</div></a>
			<a id="01-01" href="../index.php"><div class="menu-item">Retour au portail</div></a>
		</div>
	</div>

	
	<div class="menu-block" id="menu-inventaire">
		<div class="menu-titre" id="entete-menu-inventaire"><i class="icon menu-icon ion-ios7-monitor"></i>INVENTAIRE</div>
		<div class="menu-items">
			<a id="02-01" href="index.php?page=materiels"><div class="menu-item" id='materiels' >matériels</div></a>
			<a id="02-02" href="index.php?page=marques"><div class="menu-item" id='marques'>marques</div></a>
			<a id="02-03" href="index.php?page=salles"><div class="menu-item" id='salles'>salles</div></a>
			<a id="04-06" href="index.php?page=importcsv"><div class="menu-item" id='importcsv'>Importer CSV</div></a>
		</div>
	</div>
	
	
	<div class="menu-block" id="menu-dossiers">
		<div class="menu-titre" id="entete-menu-dossiers"><i class="icon menu-icon ion-medkit"></i>ASSISTANCE</div>
		<div class="menu-items">
			<a id="03-03" href="index.php?page=dossiers"><div class="menu-item" id='dossiers'>Gérer les dossiers</div></a>
		</div>
	</div>


	<div class="menu-block" id="menu-prets">
		<div class="menu-titre" id="entete-menu-prets"><i class="icon menu-icon ion-ios7-cart"></i>PRETS</div>
		<div class="menu-items">
			<a id="05-01" href="index.php?page=prets"><div class="menu-item" id='prets'>Gérer les prêts</div></a>
		</div>
	</div>
	
	
	<div class="menu-block block-06" id="menu-users">
		<div class="menu-titre" id="entete-menu-users"><i class="icon menu-icon ion-android-contacts"></i>UTILISATEURS</div>
		<div class="menu-items">
			<a id="06-01" href="index.php?page=utilisateurs"><div class="menu-item" id='utilisateurs'>Visualiser les utilisateurs</div></a>
			<a id="06-02" href="index.php?page=grades"><div class="menu-item" id='grades'>Visualiser les grades</div></a>
			<a id="06-03" href="index.php?page=importiaca"><div class="menu-item" id='importiaca'>Importer les comptes IACA</div></a>
			<a id="06-04" href="index.php?page=moncompte"><div class="menu-item" id='moncompte'>Modifier mon compte</div></a>
		</div>
	</div>
	
	<div class="menu-block" id="menu-modules">
		<div class="menu-titre" id="entete-menu-modules"><i class="icon menu-icon ion-ios7-gear"></i>MODULES</div>
		<div class="menu-items">
			<a id="07-01" href="index.php?page=recapfog"><div class="menu-item" id='recapfog'>Récapitulatif FOG</div></a>
			<a id="07-02" href="index.php?page=wol"><div class="menu-item" id='wol'>Wake On Lan</div></a>
			<a id="07-03" href="index.php?page=exportsperso"><div class="menu-item" id='exportsperso'>Export Perso</div></a>
			<a id="07-04" href="index.php?page=taginventaire"><div class="menu-item" id='taginventaire'>MAJ No Inventaire</div></a>
			<a id="07-05" href="index.php?page=imagefog"><div class="menu-item" id='imagefog'>Images Fog</div></a>
			<a id="07-06" href="index.php?page=modportail"><div class="menu-item" id='modportail'>Menu portail</div></a>
			<a id="07-07" href="index.php?page=gestfichiers"><div class="menu-item" id='gestfichiers'>Gestionnaire de fichiers</div></a>
			<a id="07-08" href="index.php?page=migfog"><div class="menu-item" id='migfog'>Migration Fog</div></a>
			<a id="" href="index.php?page=migdossiers"><div class="menu-item" id='migdossiers'>Migration dossiers</div></a>
			<a id="" href="index.php?page=geninventaire"><div class="menu-item" id='geninventaire'>Générer Inventaire</div></a>
			<a id="07-12" href="index.php?page=migusers"><div class="menu-item" id='migusers'>Migration Utilisateurs</div></a>
			<a id="" href="index.php?page=aic"><div class="menu-item" id='aic'>Création AIC</div></a>
		</div>
	</div>
		
	<div class="menu-block" id="menu-info">
		<div class="menu-titre" id="entete-menu-info"><i class="icon menu-icon ion-information-circled"></i>INFO</div>
		<div class="menu-items">
			<a id="08-01" href="index.php?page=college"><div class="menu-item" id='college'>Fiche collège</div></a>
			<a id="08-02" href="index.php?page=rss"><div class="menu-item" id='rss'>Flux RSS</div></a>
			<a id="08-04" href="index.php?page=statbat"><div class="menu-item" id='statbat'>Stats bâtons</div></a>
			<a id="08-03" href="index.php?page=statpie"><div class="menu-item" id='statpie'>Stats camemberts</div></a>
			<a id="08-05" href="index.php?page=statparc"><div class="menu-item" id='statparc'>Stats utilisation du parc</div></a>
			<a id="08-06" href="index.php?page=infoserveur"><div class="menu-item" id='infoserveur'>Info serveur</div></a>
		</div>
	</div>
	
	<div class="menu-block" id="menu-donnees">
		<div class="menu-titre" id="entete-menu-donnees"><i class="icon menu-icon ion-social-buffer"></i>DONNEES</div>
		<div class="menu-items">
			<a id="04-01" href="index.php?page=importocs"><div class="menu-item" id='importocs'>Importer DB OCS</div></a>
			<a id="04-02" href="index.php?page=exports"><div class="menu-item" id='exports'>Exports</div></a>
			<a id="04-03" href="index.php?page=dumpgespac"><div class="menu-item" id='dumpgespac'>Dump base GESPAC</div></a>
			<a id="04-04" href="index.php?page=dumpocs"><div class="menu-item" id='dumpocs'>Dump base OCS</div></a>
			<a id="04-05" href="index.php?page=logs"><div class="menu-item" id='logs'>Voir les Logs</div></a>
		</div>
	</div>

</div>


<script type="text/javascript">
	$(function () {
	
	
		// Quand on clique sur une image
		$('.menu-titre').click( function(el)  {
			// Si ce n'est pas le menu courant
			if ( $(this).parent().children(".menu-items").is(":hidden") ) {		
				$('.menu-items').hide("fast");	// On masque tout
				$(this).parent().children(".menu-items").show("fast");	// On affiche le set d'items
			}
		});
		
		
				// La page courante dans l'url
		var page = getQueryVariable("page");
		
		// On affiche le set d'item en fonction de la page
		if (page) {
			$("#" + page).parent().parent().show();	// On affiche le set d'items
			$("#" + page).toggleClass('menu-current');
		}
		
		
		
		
		// Le grade logué 
		var grade = "<?PHP echo $_SESSION['grade']; ?>";
	
		// Les droits du grade logué
		if (grade != "root") {

			var droits = jQuery.parseJSON('<?PHP echo $_SESSION['droits']; ?>');
			
			// désactive les menus où l'utilisateur n'a pas les droits
			$(".menu-block").each(function(index){
				
				var hideblock = true;
							
				$(this).find('a').each (function(id) {

					// Si le droit existe on ne masque pas et on ne masquera pas le menu-block, sinon on vire l'élément
					if ( droits["L-" + $(this).prop("id")] == "on" ) {
						hideblock = false;	// Au moins un élément est affiché, on ne masque pas l'entête
					}
					else {
						$(this).remove();
					}
					
				});
				
				// Tous les items du menu block sont masqués, on vire donc le menu-block
				if (hideblock == true) $(this).remove();
				
				
			});
		}
		
	});
</script>
