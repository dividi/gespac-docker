<?PHP
session_start();

	#fichier de modif du college

	// @@Gestion des droits sur la page
	$pageid = "08-01";	
	if ( !preg_match ("#L-$pageid#", $_SESSION['droits']) && $_SESSION['grade'] != 'root' ) exit("<div style='font-size:40px;margin:60px;'>Vous n'avez pas les droits d'accès à cette page !</div>");	// Droit d'accès en lecture à la page
	$E_chk = ($_SESSION['grade'] == 'root') ? true : preg_match ("#E-$pageid#", $_SESSION['droits']);	// Droit d'accès en écriture à la page

	// cnx à la base de données GESPAC
	$con_gespac 	= new Sql ($host, $user, $pass, $gespac);
	
	// stockage des lignes retournées par sql dans un tableau nommé avec originalité "array" (mais "tableau" peut aussi marcher)
	$college_info = $con_gespac->QueryRow ( "SELECT clg_uai, clg_nom, clg_ati, clg_ati_mail, clg_adresse, clg_cp, clg_ville, clg_tel, clg_fax, clg_site_web, clg_site_grr, clg_nbSalles, clg_nbEleves, clg_appAbsence, clg_appNote, clg_appCahiertxt, clg_appCdi  FROM college;" );

	$clg_uai 		= stripslashes($college_info ["clg_uai"]);
	$clg_nom 		= stripslashes($college_info ["clg_nom"]);
	$clg_ati 		= stripslashes($college_info ["clg_ati"]);
	$clg_ati_mail 	= stripslashes($college_info ["clg_ati_mail"]);
	$clg_adresse 	= stripslashes($college_info ["clg_adresse"]);
	$clg_cp 		= $college_info ["clg_cp"];
	$clg_ville 		= stripslashes($college_info ["clg_ville"]);
	$clg_tel 		= $college_info ["clg_tel"];
	$clg_fax 		= $college_info ["clg_fax"];
	$clg_site_web 	= stripslashes($college_info ["clg_site_web"]);
	$clg_site_grr 	= stripslashes($college_info ["clg_site_grr"]);
	$clg_nbSalles	= $college_info ["clg_nbSalles"];
	$clg_nbEleves	= $college_info ["clg_nbEleves"];
	$clg_appAbsence = stripslashes($college_info ["clg_appAbsence"]);
	$clg_appNote	= stripslashes($college_info ["clg_appNote"]);
	$clg_appCahiertxt = stripslashes($college_info ["clg_appCahiertxt"]);
	$clg_appCdi	= stripslashes($college_info ["clg_appCdi"]);

?>


<div class="entetes" id="entete-college">	

	<span class="entetes-titre">FICHE COLLEGE <i class="icon entetes-icon ion-help-circled help-button"></i></span>
	<div class="helpbox">C'est la fiche d'identité du collège.</div>

	<span class="entetes-options">
		<span class="option"><?PHP if ( $E_chk ) echo "<a href='gestion_college/form_college.php?maxheight=650&id=$clg_uai' class='editbox' title='Modifier la fiche collège'> <i class='icon entetes-icon ion-compose'></i></a>";?></span>
	</span>

</div>

<div class="spacer"></div>

	
<?PHP
		
echo "
	<center>
			<table class='smalltable alternate'>

				<tr class='tr1'>
					<TD><B>UAI</B></TD>
					<TD>$clg_uai</TD>
				</tr>
			
				<tr class='tr2'>
					<TD><B>Nom du collège</B></TD>
					<TD>$clg_nom</TD>
				</tr>
			
				<tr class='tr1'>
					<TD><B>Nom de l'ATI</B></TD>
					<TD>$clg_ati</TD>
				</tr>				
			
				<tr class='tr2'>
					<TD><B>Mail de l'ATI</B></TD>
					<TD><a href='mailto:$clg_ati_mail'>$clg_ati_mail</a></TD>
				</tr>
			
				<tr class='tr1'>
					<TD><B>Adresse du collège</B></TD>
					<TD>$clg_adresse</TD>
				</tr>
			
				<tr class='tr2'>
					<TD><B>Code Postal</B></TD>
					<TD>$clg_cp</TD>
				</tr>
			
				<tr class='tr1'>
					<TD><B>Ville</B></TD>
					<TD>$clg_ville</TD>
				</tr>
				
				<tr class='tr2'>
					<TD><B>Téléphone</B></TD>
					<TD>$clg_tel</TD>
				</tr>
			
				<tr class='tr1'>
					<TD><B>Fax</B></TD>
					<TD>$clg_fax</TD>
				</tr>
			
				<tr class='tr2'>
					<TD><B>Site web du collège</B></TD>
					<TD><a href='http://$clg_site_web' target=_blank>http://$clg_site_web</a></TD>
				</tr>				
			
				<tr class='tr1'>
					<TD><B>Accès GRR</B></TD>
					<TD><a href='http://$clg_site_grr' target=_blank>http://$clg_site_grr</a></TD>
				</tr>	
			
				<tr class='tr2'>
					<TD><B>Nombre de salles</B></TD>
					<TD>$clg_nbSalles</TD>
				</tr>
							
				<tr class='tr1'>
					<TD><B>Nombre d'élèves</B></TD>
					<TD>$clg_nbEleves</TD>
				</tr>
							
				<tr class='tr2'>
					<TD><B>Logiciel d'absences</B></TD>
					<TD>$clg_appAbsence</TD>
				</tr>
							
				<tr class='tr2'>
					<TD><B>Logiciel de notes</B></TD>
					<TD>$clg_appNote</TD>
				</tr>
							
				<tr class='tr1'>
					<TD><B>Logiciel de cahier de texte</B></TD>
					<TD>$clg_appCahiertxt</TD>
				</tr>
							
				<tr class='tr2'>
					<TD><B>Logiciel CDI</B></TD>
					<TD>$clg_appCdi</TD>
				</tr>			
			
			
			</table>
		</center>";
			
?>
