<div class="entetes" id="entete-exports">	

	<span class="entetes-titre">EXPORTS <i class="icon entetes-icon ion-help-circled help-button"></i></span>
	<div class="helpbox">Permet la création d'exports de la base GESPAC.</div>

</div>

<div class="spacer"></div>

	<?PHP
		include_once ('gestion_donnees/export_fog.php');
		include_once ('gestion_donnees/export_gespac_fog.php');
		include_once ('gestion_donnees/export_inventaire.php');
	?>
